import { Author } from "./author.model";

export interface Product{
    title:String;
    authors: Author[];
    isbn: String;
    language: String;
    productstatus:String;
    booktype: String;
    coverimage: String;
    markets:[],
    market_details:[];
}