import { Author } from "./author.model";
import { ElasticsearchProductList } from "./elasticsearchProductlist.model";

export class BookMatchElementModel{
     _id!:any;
     bookId!: String;
     title!: String;
     isbn!: String;
     authors!: Author[];
     language!: String;
     coverImage!: String;
     bookType!: String;
     productStatus!: String;
     elasticsearchProductList!: ElasticsearchProductList [];

}
