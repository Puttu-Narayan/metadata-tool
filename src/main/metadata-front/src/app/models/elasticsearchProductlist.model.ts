
export class ElasticsearchProductList{
    provider_productid!: String;
    bookId!: any;
    productid!: String;
    title!: String;
    coverimage!: String;
    isbn!: String;
    author_fname!: any[] ;
    author_lname!: any[];
    seriesName!: String;
    language!: String;
    markets!: any[];
    productstatus!: String;
    booktype!: String;
    booktypeid!: number;
}
