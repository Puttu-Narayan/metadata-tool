import { environment } from './../../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';



@Injectable({
  providedIn: 'root'
})
export class MetaService {

  private url = environment.apiUrl;

  constructor( private http: HttpClient) { }

  public getAllBookMatchList(){
      return this.http.get<any>(`${this.url}/title/sample/mongo`);
  }

  public postEditBookMatchList(item: any){
    return this.http.put<any>(`${this.url}/title/update`,item);
  }

  public putAllBookMatchList(item: any){
    return this.http.put<any>(`${this.url}`,item);
  }


}
