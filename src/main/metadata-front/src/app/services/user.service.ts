import {EventEmitter, Injectable, Output} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {of, Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private isLoggedIn: boolean;
  @Output() event$:EventEmitter<boolean>=new EventEmitter();
  constructor(private http: HttpClient) {
    this.isLoggedIn = false
  }

  login(user: any): Observable<any> {
    this.isLoggedIn = user.email=="paulo.moran@nextory.com" && user.password=="Nextory2022"
    return of(this.isLoggedIn);
  }
  register(user: any): Observable<any> {
    return this.http.post("https://reqres.in/api/register", user);
  }

  getIsLoggedIn(): boolean {
    return this.isLoggedIn;
  }

  setEventEmitter(enable:boolean) {
    this.event$.next(enable);
  }

  getEventEmitter() {
    return this.event$;
  }


}
