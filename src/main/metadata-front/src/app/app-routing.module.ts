import { MetaComponent } from './components/meta/meta.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { TitleManagementComponent } from './components/meta/title-management/title-management.component';
import {LoginComponent} from "./components/login/login.component";

const routes: Routes = [
  {
    path: '',
    component: LoginComponent,
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'login' }
    ]
  },
  {
    path: 'home',
    component: HomeComponent
  },
  { path: 'metadata', component: MetaComponent,
    children: [
      {path: 'titleManagement', component: TitleManagementComponent},
    ]
  },
  { path: '**', pathMatch: 'full', redirectTo: 'login' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
