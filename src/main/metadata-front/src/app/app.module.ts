import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { MetaComponent } from './components/meta/meta.component';
import { TitleManagementComponent } from './components/meta/title-management/title-management.component';
import { CommonModule } from '@angular/common';
import { NgbCarouselConfig, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import {UserService} from "./services/user.service";

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    MetaComponent,
    TitleManagementComponent,
    HomeComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CommonModule,
    HttpClientModule,
    FormsModule,
    NgbModule
  ],
  exports: [
    NavBarComponent
  ],
  providers: [
    NgbCarouselConfig,
    UserService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
