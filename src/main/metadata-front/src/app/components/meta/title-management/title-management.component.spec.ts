import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TitleManagementComponent } from './title-management.component';

describe('TitleManagementComponent', () => {
  let component: TitleManagementComponent;
  let fixture: ComponentFixture<TitleManagementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TitleManagementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TitleManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
