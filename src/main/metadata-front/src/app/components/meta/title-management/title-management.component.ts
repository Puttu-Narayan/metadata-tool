import { Router } from '@angular/router';
import { MetaService } from './../../../services/meta/meta.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { BookMatchElementModel } from 'src/app/models/bookMatchElement.model';
import { NgbAlert, NgbCarouselConfig, NgbModal, NgbProgressbarConfig,  } from '@ng-bootstrap/ng-bootstrap';
import {debounceTime} from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-title-management',
  templateUrl: './title-management.component.html',
  styleUrls: ['./title-management.component.css']
})
export class TitleManagementComponent implements OnInit {

  //variables
  private _success = new Subject<string>();
  private _alert = new Subject<string>();
  searchMessage:String = '';
  messageCrud:String = '';
  typeAlert:String = '';

  @ViewChild('selfClosingAlert', {static: false}) selfClosingAlert: NgbAlert | undefined;
  @ViewChild('closingAlert', {static: false}) closingAlert: NgbAlert | undefined;


  view:String = "results";
  inputDisabled:boolean = true;

  listBookElement: BookMatchElementModel[] = [];
  filterBookElement:BookMatchElementModel [] = [];
  tableResults: BookMatchElementModel[] = [];

  typeElement: String = '';
  searchOption:boolean = false;
  itemElasticSearch: any;

  positionChild:any;
  positionFather:any;

  // pagination
  pageSize = 15;
  page = 1;
  numPages=0;

  // val input
  title:String ='';
  lang:String  ='';
  group:String ='';

  //val result not found
  titleNotFound:String ='';
  langNotFound:String  ='';
  groupNotFound:String ='';

  constructor(private meta: MetaService,
              private router:Router ,
              private configCarousel: NgbCarouselConfig,
              private modalService: NgbModal,
              private configProgressbar: NgbProgressbarConfig){

                //carousel
                this.configCarousel.interval = 300000;

  }

  ngOnInit(): void {
    this.onList();
    this.onSearchMessage();
    this.onMessage();
  }

  //list on DB
  onList(){
    this.meta.getAllBookMatchList().subscribe((resp:any)=>{
      if(resp.length != 0)
        this.listBookElement = resp;
        this.inputDisabled = false;
        this.filterBookElement = resp;
        this.progressBar(resp.length);
        this.tableResults = resp;
    });
  }

  //View - results - matching(all) - sarch
  viewMatchingTool(valor: String){
    this.view = valor;
  }

  //progressbar
    progressBar(maxLength: number){
     this.configProgressbar.max = maxLength;
     this.configProgressbar.striped = true;
     this.configProgressbar.animated = true;
     this.configProgressbar.type = 'primary';
     this.configProgressbar.height = '1rem';
  }

  //filter
  onFilter(form: NgForm){

    let valueSearch = form.value;

    this.titleNotFound =valueSearch.title;
    this.langNotFound  =valueSearch.lang;
    this.groupNotFound =valueSearch.group;

    this.searchOption = true;

    this.viewMatchingTool("matching");

    let filter;

    //filter title
    if(valueSearch.title != '' && valueSearch.lang == '' && valueSearch.group == ''){
      filter = this.listBookElement.filter( function(currentElement) {
        return currentElement.title.toLowerCase() == valueSearch.title.toLowerCase();
      });
    // filter lang
    }else if(valueSearch.title == '' && valueSearch.lang != '' && valueSearch.group == ''){
      filter = this.listBookElement.filter( function(currentElement) {
        return currentElement.language.toLowerCase() == valueSearch.lang.toLowerCase();
      });

    // filter group
    }else if(valueSearch.title == '' && valueSearch.lang == '' && valueSearch.group != ''){
      filter = this.listBookElement.filter( function(currentElement) {
        console.log('search group')
        //return currentElement.group.toLowerCase() == valueSearch.lang.toLowerCase();
      });

    // filter tiitle- lang
    }else if(valueSearch.title != '' && valueSearch.lang != '' && valueSearch.group == ''){
      filter = this.listBookElement.filter( function(currentElement) {
        return currentElement.title.toLowerCase() == valueSearch.title.toLowerCase() && currentElement.language.toLowerCase() == valueSearch.lang.toLowerCase();
      });

    //filter title-group
    }else if(valueSearch.title != '' && valueSearch.lang == '' && valueSearch.group != ''){
      filter = this.listBookElement.filter( function(currentElement) {
        return currentElement.title.toLowerCase() == valueSearch.title.toLowerCase()
                //&& currentElement.group.toLowerCase() == valueSearch.group.toLowerCase()
                ;
      });

    //filter lang-group
    }else if(valueSearch.title == '' && valueSearch.lang != '' && valueSearch.group != ''){
      filter = this.listBookElement.filter( function(currentElement) {
        return currentElement.language.toLowerCase() == valueSearch.lang.toLowerCase()
               //&& currentElement.group.toLowerCase() == valueSearch.group.toLowerCase()
               ;
      });

    }else if(valueSearch.title != '' && valueSearch.lang != '' && valueSearch.group != ''){
      filter = this.listBookElement.filter( function(currentElement) {
        return currentElement.title.toLowerCase() == valueSearch.title.toLowerCase() && currentElement.language.toLowerCase() == valueSearch.lang.toLowerCase()
              // && currentElement.group.toLowerCase() == valueSearch.group.toLowerCase()
        ;
      });

    }else{
      this.onRefresh();
    }

    if(filter != undefined){
      this.tableResults = filter;
      this.filterBookElement = filter
      this.progressBar(filter.length);

      if(filter.length == 0){
        this.viewMatchingTool("search");
      }else{
        this.viewMatchingTool("search");
        this.typeAlert = 'success'
        this._alert.next(`Match found`);
      }
    }

  }

  //Message
  onSearchMessage(){
    this._success.subscribe(message => this.searchMessage = message);
    this._success.pipe(debounceTime(5000)).subscribe(() => {
      if (this.selfClosingAlert) {
        this.selfClosingAlert.close();
      }
    });
  }

  onMessage(){
    this._alert.subscribe(message => this.messageCrud = message);
    this._alert.pipe(debounceTime(5000)).subscribe(() => {
      if (this.closingAlert) {
        this.closingAlert.close();
      }
    });
  }

  //Modal
  open(content:any, item: any, typeElement:String = 'father' ,posFather:number,posChild?: number) {
    this.modalService.open(content, { size: 'lg' });

    this.itemElasticSearch = item;
    this.typeElement = typeElement;
    this.positionFather = posFather;
    if(posChild != undefined){
      this.positionChild = posChild;
    }
  }

  //Edit
  onEdit(form: NgForm, item: BookMatchElementModel, type: String){

    this.typeAlert = type;

    let newTitle = form.value.titleEdit;
    let itemEdit
    let bookIdList:any [] = [];

      if(this.typeElement == 'child'){
        this.tableResults[this.positionFather].elasticsearchProductList[this.positionChild].title = newTitle;

        itemEdit = {
          'bookId': this.tableResults[this.positionFather].elasticsearchProductList[this.positionChild].bookId,
          'newTitle': newTitle
        }
        this.meta.postEditBookMatchList(itemEdit)

      }else if(this.typeElement == 'father'){


        this.tableResults[this.positionFather].title = newTitle;
        this.tableResults[this.positionFather].elasticsearchProductList.forEach((val =>{
            val.title = newTitle;
            bookIdList.push(val.bookId);
        }));

        itemEdit = {
          'bookIdList':bookIdList,
          'newTitle': newTitle
        }

      }
      console.log(itemEdit);

    this._alert.next(`Title edited correctly: ${newTitle}`);
  }

  //Discard
  onDiscardAll(item:any, type:String){
    this.typeAlert = type;

    this._alert.next(`It was removed successfully: ${item.title}`);

    let itemAux = this.tableResults.filter( data =>{
      return data._id != item._id;
    });

    this.tableResults = itemAux;
    this.filterBookElement = itemAux;

  }

  onRefresh(){
    if(this.view == 'search'){
      this.view = 'matching';
    }
    this.inputDisabled = true;
    this.tableResults = [];
    this.filterBookElement = [];
    this.onList();
  }

  onClear(option:String){
    if(option == 'title'){
      this.title = '';
    }else if(option =='lang'){
      this.lang = '';
    }else if(option == 'group'){
      this.group = ''
    }else{
      this.title = ''
      this.lang = ''
      this.group = ''
    }
  }

}


