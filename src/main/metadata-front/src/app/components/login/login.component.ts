import { Component, OnInit } from '@angular/core';
import {AuthService} from "../../services/meta/auth.service";
import {Router} from "@angular/router";
import {UserService} from "../../services/user.service";
import {NavBarComponent} from "../nav-bar/nav-bar.component";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  email: string;
  password: string;
  isLoggedIn: boolean;

  constructor(public router: Router, public userService: UserService) {
    this.email = ""
    this.password = ""
    this.isLoggedIn = false
  }

  ngOnInit(): void {
  }

  login(email: String, password: String){
    this.isLoggedIn = email=="paulo.moran@nextory.com" && password=="Nextory2022"
    const user = {email: this.email, password: this.password};
    this.userService.login(user).subscribe( data => {
      this.isLoggedIn=data;
      if(this.isLoggedIn){
        this.userService.setEventEmitter(this.isLoggedIn)
        console.log(this.userService.getIsLoggedIn())
        this.router.navigate(['home']);
      }
    });
  }

  getIsLoggedIn(): boolean{
    return this.isLoggedIn
  }

}
