import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {of, Observable} from "rxjs";
import {LoginComponent} from "../login/login.component";
import {UserService} from "../../services/user.service";

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {
  public _isLoggedIn$: boolean;

  constructor(private router: Router, public userService: UserService) {
    this._isLoggedIn$ = false;
  }

  ngOnInit(): void {
    this.userService.getEventEmitter().subscribe( data => {
      this._isLoggedIn$=data;
    });
  }

  onNavigate(link: String){
    this.router.navigate([link]);
  }

}
