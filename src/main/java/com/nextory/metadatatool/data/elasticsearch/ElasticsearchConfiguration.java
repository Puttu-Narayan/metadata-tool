package com.nextory.metadatatool.data.elasticsearch;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import java.util.Objects;

@Configuration
public class ElasticsearchConfiguration {
    private static final Logger logger = LoggerFactory.getLogger(ElasticsearchConfiguration.class);

    @Value("${elasticsearch.host}")
    private static String elasticsearchHost = "10.91.0.11";

    @Value("${elasticsearch.clusterName}")
    private static String elasticsearchClusterName = "elasticsearch";

    @Value("${elasticsearch.port}")
    private static Integer elasticsearchPort = 9200;

    private static RestHighLevelClient elasticsearchClient;

    public static RestHighLevelClient getElasticSearchClient() {
        if(Objects.nonNull(elasticsearchClient )){
            return elasticsearchClient;
        }

        elasticsearchClient = new RestHighLevelClient(RestClient.builder(new HttpHost(elasticsearchHost, elasticsearchPort, "http")));

        logger.info( "Connection " + elasticsearchClusterName + "@" + elasticsearchHost+ ":" + elasticsearchPort+ " established!" );
        return elasticsearchClient;
    }
}
