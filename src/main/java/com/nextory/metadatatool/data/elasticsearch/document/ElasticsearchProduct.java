package com.nextory.metadatatool.data.elasticsearch.document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ElasticsearchProduct {
    private String provider_productid;
    private Long bookId;
    private String productid;
    private String title;
    private String coverimage;
    private String isbn;
    private List<String> author_fname;
    private List<String> author_lname;
    private String seriesName;
    private String language;
    private List<String> markets;
    private String productstatus;
    private String booktype;
    private Integer booktypeid;

    public String getProvider_productid() {
        return provider_productid;
    }

    public void setProvider_productid(String provider_productid) {
        this.provider_productid = provider_productid;
    }

    public Long getBookId() {
        return bookId;
    }

    public void setBookId(Long bookId) {
        this.bookId = bookId;
    }

    public String getProductid() {
        return productid;
    }

    public void setProductid(String productid) {
        this.productid = productid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCoverimage() {
        return coverimage;
    }

    public void setCoverimage(String coverimage) {
        this.coverimage = coverimage;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public List<String> getAuthor_fname() {
        return author_fname;
    }

    public void setAuthor_fname(List<String> author_fname) {
        this.author_fname = author_fname;
    }

    public List<String> getAuthor_lname() {
        return author_lname;
    }

    public void setAuthor_lname(List<String> author_lname) {
        this.author_lname = author_lname;
    }

    public String getSeriesName() {
        return seriesName;
    }

    public void setSeriesName(String seriesName) {
        this.seriesName = seriesName;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public List<String> getMarkets() {
        return markets;
    }

    public void setMarkets(List<String> markets) {
        this.markets = markets;
    }

    public String getProductstatus() {
        return productstatus;
    }

    public void setProductstatus(String productstatus) {
        this.productstatus = productstatus;
    }

    public String getBooktype() {
        return booktype;
    }

    public void setBooktype(String booktype) {
        this.booktype = booktype;
    }

    public Integer getBooktypeid() {
        return booktypeid;
    }

    public void setBooktypeid(Integer booktypeid) {
        this.booktypeid = booktypeid;
    }
}
