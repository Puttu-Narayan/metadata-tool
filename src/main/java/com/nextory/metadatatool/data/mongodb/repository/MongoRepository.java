package com.nextory.metadatatool.data.mongodb.repository;

import com.mongodb.DBObject;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoIterable;
import com.mongodb.client.result.UpdateResult;
import com.nextory.metadatatool.data.elasticsearch.document.ElasticsearchProduct;
import com.nextory.metadatatool.model.Product;
import com.nextory.metadatatool.model.ProductMatchElement;
import org.bson.Document;
import org.elasticsearch.action.update.UpdateResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.*;
import org.springframework.data.support.PageableExecutionUtils;
import org.springframework.stereotype.Repository;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

@Repository
public class MongoRepository {
    private final static String PRODUCT_MATCH_ELEMENT_COLLECTION_NAME = "productMatchElement";

    @Autowired
    MongoTemplate mongoTemplate;

    public Product save(Product prod){
        return mongoTemplate.save(prod);
    }

    public ProductMatchElement save(ProductMatchElement prod){
        return mongoTemplate.save(prod);
    }

    public boolean upsert(Update update, ProductMatchElement productMatchElement){
        Query query = new Query(Criteria.where("bookId").is(productMatchElement.getBookId()));
        UpdateResult response = mongoTemplate.upsert(query, update, ProductMatchElement.class);
        return response.wasAcknowledged();
    }

    public List<Product> find(){
        return mongoTemplate.findAll(Product.class);
    }

    public List<ProductMatchElement> findAllProductMatchElement(){
        return mongoTemplate.findAll(ProductMatchElement.class);
    }

    public List<Object> findAllProductMatchElementMongoObject(){
        List<Object> objectList = new ArrayList<>();
        MongoCollection<Document> collection = mongoTemplate.getCollection(PRODUCT_MATCH_ELEMENT_COLLECTION_NAME);
        MongoIterable iterable = collection.find();
        MongoCursor cursor = iterable.iterator();
        while (cursor.hasNext()){
            objectList.add(cursor.next());
        }
        return objectList;
    }

    public int findAllCount(){
        Query query = new Query();
        int sizeAll = (int) mongoTemplate.count(query,Product.class);

        return sizeAll;
    }

    public Page<Product> findAllProduct(Pageable pageable, long skip, int limit){
        Query query = new Query(Criteria.where("productstatus").is("ACTIVE")).with(pageable);
        List<Product> list = mongoTemplate.find(query.limit(limit).skip(skip),Product.class);

        return PageableExecutionUtils.getPage(list,pageable,()-> mongoTemplate.count(Query.of(query).limit(-1).skip(-1),Product.class));
    }

    public List<Product> findByLanguage(String lang){
        Query query = new Query(Criteria.where("language").gte(lang));
        return mongoTemplate.find(query.limit(100), Product.class);
    }

    public List<Product> findByMarket(String mkt){
        Query query = new Query(Criteria.where("markets").gte(mkt.toUpperCase()));

        return mongoTemplate.find(query.limit(100), Product.class);
    }

    public List<Product> findByTitle(String title){

        Query query = new Query(Criteria.where("title").gte(title));
        return mongoTemplate.find(query.limit(100), Product.class);
    }

    public Page<Product> findByTitlePaginate(String title, Pageable pageable){
        Query query = new Query(Criteria.where("title").regex(title)).with(pageable);

        List<Product> list = mongoTemplate.find(query,Product.class);

        return PageableExecutionUtils.getPage(list,pageable,()-> mongoTemplate.count(Query.of(query).limit(-1).skip(-1),Product.class));
    }

    public List<Product> findByAuthor(String author){

        Query query = new Query(Criteria.where("authors.firstname").is(author));
        return mongoTemplate.find(query, Product.class);
    }

    public List<Product> findByLanguageMarket(String lang,String mkt){
        Query query = new Query(Criteria.where("language").is(lang).and("markets").is(mkt.toUpperCase()));
        return mongoTemplate.find(query.limit(100), Product.class);
    }

    public List<Product> findByLanguageAuthor(String lang,String author){
        Query query = new Query(Criteria.where("language").is(lang).and("authors.firstname").is(author));
        System.out.println(query);
        return mongoTemplate.find(query.limit(100), Product.class);
    }

    public List<Product> findWithLimit(Integer limit){
        Query query = new Query();
        return mongoTemplate.find(query.limit(limit), Product.class);
    }

    public List<Product> findWithCursor(){
        Query query = new Query().noCursorTimeout().cursorBatchSize(100);
        return mongoTemplate.find(query, Product.class);
    }

    public void clearAllProductMatchElement() {
        mongoTemplate.dropCollection(ProductMatchElement.class);
    }

    public Product findProductByBookId(Long bookId) {
        Query query = new Query(Criteria.where("bookId").is(bookId));
        return mongoTemplate.findOne(query, Product.class);
    }

    public Product findProductById(String bookId) {
        Query query = new Query(Criteria.where("bookId").is(bookId));
        mongoTemplate.findById(bookId, ProductMatchElement.class);
        return mongoTemplate.findOne(query, Product.class);
    }

    public ProductMatchElement findProductMatchElementByBookId(String bookId) {
        Query query = new Query(Criteria.where("bookId").is(bookId));
        return mongoTemplate.findOne(query, ProductMatchElement.class);
    }
}
