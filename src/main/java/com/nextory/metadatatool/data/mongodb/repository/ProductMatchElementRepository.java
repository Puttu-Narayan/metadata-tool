package com.nextory.metadatatool.data.mongodb.repository;

import com.nextory.metadatatool.model.ProductMatchElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class ProductMatchElementRepository {

    @Autowired
    MongoTemplate mongoTemplate;

    public ProductMatchElement save(ProductMatchElement productMatchElement){
        return  mongoTemplate.save(productMatchElement);
    }
}
