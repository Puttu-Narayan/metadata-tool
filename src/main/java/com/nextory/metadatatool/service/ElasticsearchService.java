package com.nextory.metadatatool.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.nextory.metadatatool.data.elasticsearch.ElasticsearchConfiguration;
import com.nextory.metadatatool.data.elasticsearch.document.ElasticsearchProduct;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.Fuzziness;
import org.elasticsearch.index.query.*;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ElasticsearchService {
    private static final ObjectMapper objectMapper = new ObjectMapper();
    private final Gson gson = new Gson();
    private final RestHighLevelClient client;

    public ElasticsearchService() {
        this.client = ElasticsearchConfiguration.getElasticSearchClient();
    }

    public ElasticsearchService(RestHighLevelClient client) {
        this.client = client;
    }

    public String searchMatchTitle(String title){
        MatchQueryBuilder matchQueryBuilder = QueryBuilders.matchQuery("title", title).operator(Operator.AND);
        List<ElasticsearchProduct> products = getProductListFromSearchRequest(
                createSearchRequestFromQueryBuilder(matchQueryBuilder));
        return convertProductListToJson(products);
    }

    public List<ElasticsearchProduct> searchFuzzyTitle(String title){
        FuzzyQueryBuilder fuzzyQueryBuilder = QueryBuilders.fuzzyQuery("title", title)
                .fuzziness(Fuzziness.ONE).maxExpansions(5);
        List<ElasticsearchProduct> products = getProductListFromSearchRequest(
                createSearchRequestFromQueryBuilder(fuzzyQueryBuilder));
        return products;
    }

    private SearchRequest createSearchRequestFromQueryBuilder(QueryBuilder builder){
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder().postFilter(builder);
        SearchRequest searchRequest = new SearchRequest();
        searchRequest.source(searchSourceBuilder);
        return searchRequest;
    }

    private List<ElasticsearchProduct> getProductListFromSearchRequest(SearchRequest searchRequest){
        try {
            SearchResponse response = client.search(searchRequest, RequestOptions.DEFAULT);
            List<ElasticsearchProduct> elasticsearchProducts = new ArrayList<>(response.getHits().getHits().length);
            for(SearchHit searchHit: response.getHits().getHits()){
                elasticsearchProducts.add(objectMapper.readValue(
                        searchHit.getSourceAsString(), ElasticsearchProduct.class));
            }
            return elasticsearchProducts;
        } catch (Exception e){
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    private String convertProductListToJson(List<ElasticsearchProduct> products){
        return gson.toJson(products);
    }
}
