package com.nextory.metadatatool.service;

import com.nextory.metadatatool.model.NestPayload;
import com.nextory.metadatatool.model.Product;
import org.springframework.stereotype.Service;

@Service
public class NestService {
    private MongoService mongoService;

    public NestService(MongoService mongoService) {
        this.mongoService = mongoService;
    }

    public NestPayload getNestPayload(Long bookId, String newTitle) {
        Product product = mongoService.getProductByBookId(bookId);
        NestPayload nestPayload = buildKafkaObject(product, newTitle);
        return nestPayload;
    }

    private NestPayload buildKafkaObject(Product product, String newTitle) {
        return new NestPayload(product, newTitle);
    }
}
