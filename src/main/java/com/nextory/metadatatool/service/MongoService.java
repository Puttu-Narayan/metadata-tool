package com.nextory.metadatatool.service;

import com.nextory.metadatatool.model.Product;
import com.nextory.metadatatool.data.mongodb.repository.MongoRepository;
import com.nextory.metadatatool.model.ProductMatchElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MongoService {
    @Autowired
    MongoRepository mongoRepository;

    private static final int pageSize = 2000;

    public Product save(Product prod){
        return mongoRepository.save(prod);
    }

    public ProductMatchElement save(ProductMatchElement prod){
        return mongoRepository.save(prod);
    }

    public List<Product> getAll(){
        return mongoRepository.find();
    }

    public List<ProductMatchElement> getAllProductMatchElement(){
        return mongoRepository.findAllProductMatchElement();
    }

    public List<Object> getAllProductMatchElementObjectList(){
        return mongoRepository.findAllProductMatchElementMongoObject();
    }

    public int getAllProductCount(){
        return mongoRepository.findAllCount();
    }

    public static int getPageSize(){return pageSize;}

    public List<Product> getAllProductPaginate() {
        long startTimeQuery = System.currentTimeMillis();
        long totalReg = 0;
        int allReg = getAllProductCount();
        List<Product> list = new ArrayList<>();

        for (int pageNo = 0; pageNo <= allReg/pageSize; pageNo++) {
            long startPageNo = System.currentTimeMillis();
            Pageable paging = PageRequest.of(pageNo, pageSize);
            Page<Product> pagedResult = mongoRepository.findAllProduct(paging,totalReg, pageSize);

            if(pagedResult.hasContent()) {

                list.addAll(pagedResult.getContent());
                totalReg += pagedResult.getContent().size();
                long endTimePageNo = System.currentTimeMillis() - startPageNo;
                System.out.println("totalReg: " + totalReg);
                System.out.println("totalTimePage "+pageNo+": " + ((endTimePageNo/1000)/60)+":"+((startPageNo/1000)%60));
            } else {
                new ArrayList<Product>();
                long endTimePageNo = System.currentTimeMillis() - startPageNo;
                System.out.println("totalTimePage "+pageNo+": " + ((endTimePageNo/1000)/60)+":"+((startPageNo/1000)%60));
            }
        }

        long endTimeQuery = System.currentTimeMillis() - startTimeQuery;

        System.out.println("totalTime: " + ((endTimeQuery/1000)/60)+":"+((endTimeQuery/1000)%60));
        System.out.println("totalReg: "+totalReg);
        return list;
    }

    public List<Product> getAllProductByPage(int pageNo, Long totalReg) {
        long startTimeQuery = System.currentTimeMillis();
        List<Product> list = new ArrayList<>();
        Pageable paging = PageRequest.of(pageNo, pageSize);
        Page<Product> pagedResult = mongoRepository.findAllProduct(paging,totalReg, pageSize);

        if(pagedResult.hasContent()) {
            list.addAll(pagedResult.getContent());
            totalReg += pagedResult.getContent().size();
            System.out.println("totalReg: " + totalReg);
        } else {
            new ArrayList<Product>();
        }
        long endTimeQuery = System.currentTimeMillis() - startTimeQuery;

        System.out.println("totalTime: " + ((endTimeQuery/1000)/60)+":"+((endTimeQuery/1000)%60));
        System.out.println("totalReg: "+totalReg);
        return list;
    }

    public Product update(Product prod){
        return mongoRepository.save(prod);
    }

    public List<Product> getByLanguage(String lang){
        return mongoRepository.findByLanguage(lang);
    }

    public List<Product> getByMarket(String mkt){
        return mongoRepository.findByMarket(mkt);
    }

    public List<Product> getByTitle(String title){
        return mongoRepository.findByTitle(title);
    }

    public List<String> getTitlePaginable(String title){
        long startTimeQuery = System.currentTimeMillis();
        int pageSize = 2500;
        long totalReg = 0;

        int count = mongoRepository.findAllCount();
        List<String> list = new ArrayList<>();

        for (int pageNo = 0; pageNo <= count/pageSize; pageNo++) {
            long startPageNo = System.currentTimeMillis();
            Pageable paging = PageRequest.of(pageNo, pageSize);
            Page<Product> pagedResult = mongoRepository.findByTitlePaginate(title,paging);

            if(pagedResult.hasContent()) {
                list.add(pagedResult.getContent().toString());
                totalReg += pagedResult.getContent().size();
                long endTimePageNo = System.currentTimeMillis() - startPageNo;
                System.out.println("totalTimePage "+pageNo+": " + ((endTimePageNo/1000)/60)+":"+((startPageNo/1000)%60));
            } else {
                new ArrayList<Product>();
                long endTimePageNo = System.currentTimeMillis() - startPageNo;
                System.out.println("totalTimePage "+pageNo+": " + ((endTimePageNo/1000)/60)+":"+((startPageNo/1000)%60));
            }
        }

        long endTimeQuery = System.currentTimeMillis() - startTimeQuery;

        System.out.println("totalTime: " + ((endTimeQuery/1000)/60)+":"+((endTimeQuery/1000)%60));
        System.out.println("totalReg: "+totalReg);
        return list;
    }

    public List<Product> getByAuthor(String author){
        return mongoRepository.findByAuthor(author);
    }

    public List<Product> getByLanguageMarket(String lang, String mkt){
        return mongoRepository.findByLanguageMarket(lang,mkt);
    }

    public List<Product> getByLanguageAuthor(String lang, String author){
        return mongoRepository.findByLanguageAuthor(lang,author);
    }

    public List<Product> getWithLimit(Integer limit){
        return mongoRepository.findWithLimit(limit);
    }

    public void clearProductMatchElement() {
        mongoRepository.clearAllProductMatchElement();
    }

    public Product getProductByBookId(Long bookId) {
        return mongoRepository.findProductByBookId(bookId);
    }

    public ProductMatchElement getProductMatchElementByBookId(String bookId) {
        return mongoRepository.findProductMatchElementByBookId(bookId);
    }

    public boolean upsert(ProductMatchElement productMatchElement) {
        Update update = new Update().set("status", productMatchElement.getStatus());
        return mongoRepository.upsert(update, productMatchElement);
    }
}
