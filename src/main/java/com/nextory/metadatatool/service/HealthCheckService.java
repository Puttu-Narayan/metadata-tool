package com.nextory.metadatatool.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;

@Service("healthservice")
public class HealthCheckService {
    public static final Logger _logger = LoggerFactory.getLogger(HealthCheckService.class);

    @Autowired
    private MongoService mongoService;

    public HealthCheckService(MongoService mongoService) {
        this.mongoService = mongoService;
    }

    @SuppressWarnings("null")
    public int checkSystemHealth() {
        try{
            mongoService.getAllProductCount();
            return ( HttpServletResponse.SC_OK );
        }catch(Exception e){
            return ( HttpServletResponse.SC_INTERNAL_SERVER_ERROR );
        }
    }
}
