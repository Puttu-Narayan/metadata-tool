package com.nextory.metadatatool.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.nextory.metadatatool.data.elasticsearch.document.ElasticsearchProduct;
import com.nextory.metadatatool.model.Product;
import com.nextory.metadatatool.model.ProductMatchElement;
import com.nextory.metadatatool.model.ProductMatchElementStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class ProductMatchService {
    public static final int SAMPLE_RESULT_BATCH_SIZE = 2000;
    protected final Gson gson;
    @Autowired
    protected final ElasticsearchService elasticsearchService;
    @Autowired
    protected final MongoService mongoService;

    /*  CONSTRUCTORS */

    public ProductMatchService() {
        elasticsearchService = new ElasticsearchService();
        mongoService = new MongoService();
        gson = new GsonBuilder().setPrettyPrinting().create();
    }

    public ProductMatchService(ElasticsearchService elasticsearchService, MongoService mongoService) {
        this.elasticsearchService = elasticsearchService;
        this.mongoService = mongoService;
        gson = new GsonBuilder().setPrettyPrinting().create();
    }

    /*  METHODS */

    public String getBookMatchListByTitle(){
        List<ProductMatchElement> productMatchElementList = new ArrayList<>(mongoService.getAllProductCount());
        List<Product> productList = new ArrayList<>();
        int page = 0;
        long skip = 0L;
        do {
            skip += productList.size();
            productList = mongoService.getAllProductByPage(page, skip);
            for(Product product: productList) {
                List<ElasticsearchProduct> elasticSearchList = getElasticSearchProductListByProduct(product);
                productMatchElementList.add(
                        new ProductMatchElement(
                                product.get_id(), product.getTitle(), product.getAuthors(), product.getLanguage(), product.getIsbn(),
                                elasticSearchList, product.getCoverimage(), product.getBooktype().getBook_type() , product.getProductstatus())
                );
            }
            page++;
        } while (productList.size()>0);

        return gson.toJson(productMatchElementList);
    }

    public String getBookMatchListSample(){
        List<ProductMatchElement> productMatchElementList = new ArrayList<>(SAMPLE_RESULT_BATCH_SIZE);
        List<Product> productList = mongoService.getWithLimit(SAMPLE_RESULT_BATCH_SIZE);
        for(Product product: productList){
            List<ElasticsearchProduct> elasticSearchList = getElasticSearchProductListByProduct(product);
            if (!elasticSearchList.isEmpty()){
                ProductMatchElement productMatchElement = new ProductMatchElement(
                        product.get_id(), product.getTitle(), product.getAuthors(), product.getLanguage(), product.getIsbn(),
                        elasticSearchList, product.getCoverimage(), product.getBooktype().getBook_type(), product.getProductstatus());
                productMatchElementList.add(productMatchElement);
            }
        }
        return gson.toJson(productMatchElementList);
    }

    public String getBookMatchObjectList(){
        List<Object> productMatchElementList;
        productMatchElementList = mongoService.getAllProductMatchElementObjectList();
        return gson.toJson(productMatchElementList);
    }

    protected List<ElasticsearchProduct> getElasticSearchProductListByProduct(Product product){
        List<ElasticsearchProduct> elasticsearchProductList;
        elasticsearchProductList = elasticsearchService.searchFuzzyTitle(product.getTitle());
        if(elasticsearchProductList.size()>0){
            elasticsearchProductList = elasticsearchProductList.stream().filter(e -> isSuitableProductForResultList(e, product)).collect(Collectors.toList());
        }
        return elasticsearchProductList;
    }

    private boolean isSuitableProductForResultList(ElasticsearchProduct elasticsearchProduct, Product product){
        return !elasticsearchProduct.getTitle().equals(product.getTitle()) && !elasticsearchProduct.getIsbn().equals(product.getIsbn())
                && isSameAuthor(elasticsearchProduct, product)
                && elasticsearchProduct.getLanguage().equals(product.getLanguage());
    }

    private boolean isSameAuthor(ElasticsearchProduct elasticsearchProduct, Product product){
        return isSameName(elasticsearchProduct, product) && isSameLastName(elasticsearchProduct, product);
    }

    private boolean isSameName(ElasticsearchProduct elasticsearchProduct, Product product){
            return !Objects.isNull(elasticsearchProduct.getAuthor_fname()) && !Objects.isNull(product.getAuthors()) &&
                    elasticsearchProduct.getAuthor_fname().stream().findFirst().isPresent() && product.getAuthors().stream().findFirst().isPresent() &&
                    elasticsearchProduct.getAuthor_fname().stream().findFirst().get().equals(
                            product.getAuthors().stream().findFirst().get().getFirstname());
    }

    private boolean isSameLastName(ElasticsearchProduct elasticsearchProduct, Product product){
        return !Objects.isNull(elasticsearchProduct.getAuthor_lname()) && !Objects.isNull(product.getAuthors()) &&
                elasticsearchProduct.getAuthor_lname().stream().findFirst().isPresent() && product.getAuthors().stream().findFirst().isPresent() &&
                elasticsearchProduct.getAuthor_lname().stream().findFirst().get().equals(
                        product.getAuthors().stream().findFirst().get().getLastname());
    }

    public void discardProductMatchElement(String bookId) {
        ProductMatchElement productMatchElement = mongoService.getProductMatchElementByBookId(bookId);
        productMatchElement.setStatus(ProductMatchElementStatus.DISCARDED);
        mongoService.upsert(productMatchElement);
    }
}
