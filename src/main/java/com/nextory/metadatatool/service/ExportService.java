package com.nextory.metadatatool.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.nextory.metadatatool.model.ProductMatchElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ExportService {
    @Autowired
    MongoService mongoService;
    Gson gson = new GsonBuilder().setPrettyPrinting().create();

    public String getProductMatchListJson(){
        List<ProductMatchElement> productMatchElementList = mongoService.getAllProductMatchElement();
        return gson.toJson(productMatchElementList);
    }

}
