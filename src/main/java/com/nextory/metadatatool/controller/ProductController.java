package com.nextory.metadatatool.controller;

import com.nextory.metadatatool.model.Product;
import com.nextory.metadatatool.service.MongoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.List;

@RestController
@RequestMapping("/product")
public class ProductController {
    private final MongoService mongoService;

    public ProductController(MongoService mongoService) {
        this.mongoService = mongoService;
    }

    @PostMapping("/")
    public Product save(@RequestBody Product prod){
        return mongoService.save(prod);
    }

    @GetMapping("/")
    public List<Product> getAll(){

        System.out.println("all");
        return mongoService.getAll();
    }

    @GetMapping("/all/")
    //public List<Product> getAllProduct(@RequestParam(defaultValue = "0") Integer pageNo, @RequestParam(defaultValue = "2") Integer pageSize){
    public List<Product> getAllProduct(){
        System.out.println("all_pageable");
        return mongoService.getAllProductPaginate();
    }

    @PutMapping("/")
    public Product update(@RequestBody Product prod){
        return mongoService.update(prod);
    }

    @GetMapping("/language/")
    public List<Product> getLanguage(@PathParam("lang") String lang){
        System.out.println("lang");
        return mongoService.getByLanguage(lang);
    }

    @GetMapping("/market/")
    public List<Product> getMarket(@PathParam("market") String mkt){
        System.out.println("market");
        return mongoService.getByMarket(mkt);
    }

    @GetMapping("/title/")
    public List<Product> getTitle(@PathParam("title") String title){
        System.out.println("title");
        return mongoService.getByTitle(title);
    }

    @GetMapping("/title/pageable")
    public List<String> getTitlePaginate(@PathParam("title") String title){
        List<String> list = mongoService.getTitlePaginable(title);
        return (List<String>) new ResponseEntity<List<String>>(list, new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("/author/")
    public List<Product> getAuthor(@PathParam("author") String author){
        System.out.println("author");
        return mongoService.getByAuthor(author);
    }

    @GetMapping("/languageMarket/")
    public List<Product> getLanguageMarket(@PathParam("language") String lang,@PathParam("market") String mkt){
        System.out.println("language_market");
        return mongoService.getByLanguageMarket(lang,mkt);
    }

    @GetMapping("/languageAuthor/")
    public List<Product> getLanguageAuthor(@PathParam("language") String lang,@PathParam("author") String author){
        System.out.println("language_Author");
        return mongoService.getByLanguageAuthor(lang,author);
    }


}
