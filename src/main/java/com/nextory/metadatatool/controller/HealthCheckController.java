package com.nextory.metadatatool.controller;

import com.nextory.metadatatool.service.HealthCheckService;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import org.slf4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class HealthCheckController {
    public static final Logger _logger = LoggerFactory.getLogger(HealthCheckController.class);

    @Autowired
    private HealthCheckService healthservice;

    public HealthCheckController(HealthCheckService healthservice) {
        this.healthservice = healthservice;
    }

    @RequestMapping( value = "/howami", method = RequestMethod.GET)
    public ModelAndView checkSystem(HttpServletRequest req, HttpServletResponse res){
        ModelAndView mv = null;
        int status =  healthservice.checkSystemHealth();
        if(status == HttpServletResponse.SC_OK){
            res.setHeader("X-Custom-Header", "alive");
            res.setHeader("X-App-Header", "metatool");
            if( System.getenv("MY_POD_NAME") != null )
                res.setHeader("X-Host-Header", System.getenv("MY_POD_NAME"));
        }
        return mv;
    }


}
