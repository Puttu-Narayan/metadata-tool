package com.nextory.metadatatool.controller;

import com.nextory.metadatatool.model.NestPayload;
import com.nextory.metadatatool.service.MongoService;
import com.nextory.metadatatool.service.NestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.client.RestTemplate;

public class NestController {
    @Value("${nest.server}")
    private String nestServer;
    private final String uri = "http://"+nestServer+"/nest_content_processor/admin/actions";
    @Autowired
    private NestService nestService;

    public void sendNestPayloadToNest(Long bookId, String newTitle){
        RestTemplate restTemplate = new RestTemplate();
        String result = restTemplate.postForObject(uri, nestService.getNestPayload(bookId, newTitle), String.class);

        System.out.println(result);

    }
}
