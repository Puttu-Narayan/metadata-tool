package com.nextory.metadatatool.controller;

import com.google.gson.Gson;
import com.nextory.metadatatool.executor.ProductMatchExecutorService;
import com.nextory.metadatatool.service.ProductMatchService;
import com.nextory.metadatatool.service.ElasticsearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/elastic")
public class ElasticsearchController {
    private final ElasticsearchService service;

    public ElasticsearchController(ElasticsearchService service) {
        this.service = service;
    }

    @RequestMapping(value = "/title", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String searchTitle(@RequestParam String title){
        return service.searchMatchTitle(title);
    }
}
