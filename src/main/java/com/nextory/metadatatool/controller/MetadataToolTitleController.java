package com.nextory.metadatatool.controller;

import com.nextory.metadatatool.executor.ProductMatchExecutorService;
import com.nextory.metadatatool.service.ExportService;
import org.apache.http.HttpResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class MetadataToolTitleController {
    private final ProductMatchExecutorService productMatchService;
    private final ExportService exportService;

    public MetadataToolTitleController(ProductMatchExecutorService productMatchService, ExportService exportService) {
        this.productMatchService = productMatchService;
        this.exportService = exportService;
    }

    @RequestMapping(value = "/title/list", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String searchFuzzyMatchListByTitle(){return productMatchService.getBookMatchListByTitle();}

    @RequestMapping(value = "/title/sample", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String searchFuzzyMatchListByTitleSample(){return productMatchService.getBookMatchListSample();}

    @RequestMapping(value = "/title/sample/mongo", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getMatchListObjectByTitle(){return productMatchService.getBookMatchObjectList();}

    @RequestMapping(value = "/title/list/multithreading", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String searchFuzzyTitleMultithreading(){return productMatchService.getBookMatchListWithThreadPool();}

    @RequestMapping(value = "/title/list/export", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<byte[]> exportFuzzyMatchListByTitle(){
        String jsonResponse = exportService.getProductMatchListJson();
        byte[] customerJsonBytes = jsonResponse.getBytes();
        return ResponseEntity
                .ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=productMatchElement.json")
                .contentType(MediaType.APPLICATION_JSON)
                .contentLength(customerJsonBytes.length)
                .body(customerJsonBytes);
    }

    @RequestMapping(value = "/title/list/save")
    @ResponseBody
    public ResponseEntity<Object> saveProductMatchElementList(){
        productMatchService.storeMatchResultsInDatabase();
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/title/discard")
    @ResponseBody
    public ResponseEntity<Object> discardProductMatchElement(String bookId){
        productMatchService.discardProductMatchElement(bookId);
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/title/save")
    @ResponseBody
    public ResponseEntity<Object> updateProductMatchElementList(){
        productMatchService.storeMatchResultsInDatabase();
        return ResponseEntity.ok().build();
    }

}
