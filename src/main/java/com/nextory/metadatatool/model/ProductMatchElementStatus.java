package com.nextory.metadatatool.model;

public enum ProductMatchElementStatus {
    PROCESSED, PENDING, DISCARDED
}
