package com.nextory.metadatatool.model;

public class BookType {
    int book_type_id;
    String book_type;

    public BookType(int book_type_id, String book_type) {
        this.book_type_id = book_type_id;
        this.book_type = book_type;
    }

    public int getBook_type_id() {
        return book_type_id;
    }

    public void setBook_type_id(int book_type_id) {
        this.book_type_id = book_type_id;
    }

    public String getBook_type() {
        return book_type;
    }

    public void setBook_type(String book_type) {
        this.book_type = book_type;
    }
}

