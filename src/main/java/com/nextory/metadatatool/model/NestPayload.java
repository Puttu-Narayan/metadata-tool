package com.nextory.metadatatool.model;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class NestPayload {
    private String eventtype = "ADMIN_DATA_EDIT";
    private String identifier; //isbn
    private String src; //publisher group
    private String asrc;
    private String providerid = "764c73f9a06b4f31b146c9978421a906";
    private String format; //book format
    private String NEST_EXCHANGE_SOURCE_ID;
    private Map datatoprocess;
    private Long eventid;
    private String eventname = "ADMIN_DATA_EDIT";
    private String status = "CREATED";
    private Long eventtime; //timestamp
    private String comments = "";
    private String createdby = "paulo.moran@nextory.com";
    private String kafkaTopic = "nestadminrequest";

    public NestPayload(Product product, String newTitle){
        this.identifier = product.getIsbn();
        this.format = product.getBooktype().getBook_type();
        this.eventid = new Date().getTime();
        this.eventtime = new Date().getTime();
        HashMap data = new HashMap();
        data.put("title", newTitle);
        this.datatoprocess = data;
    }

}
