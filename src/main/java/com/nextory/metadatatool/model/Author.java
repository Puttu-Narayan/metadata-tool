package com.nextory.metadatatool.model;

public class Author {
    String firstname;
    String lastname;

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }
}
