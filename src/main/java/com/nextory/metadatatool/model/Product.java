package com.nextory.metadatatool.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document
public class Product {

    @Id
    private String _id;

    private String title;
    private List<Author> authors;
    private String isbn;
    private String language;
    private String productstatus;
    private BookType booktype;
    private String coverimage;
    private Object markets;
    private Object market_details;

    public Product() {
    }

    public Product(String _id, String title, List<Author> authors, String isbn, String language, String productstatus, BookType booktype, String coverimage, Object markets, Object market_details) {
        this._id = _id;
        this.title = title;
        this.authors = authors;
        this.isbn = isbn;
        this.language = language;
        this.productstatus = productstatus;
        this.booktype = booktype;
        this.coverimage = coverimage;
        this.markets = markets;
        this.market_details = market_details;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String get_id() {
        return _id;
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public String getIsbn() {
        return isbn;
    }

    public String getLanguage() {
        return language;
    }

    public String getProductstatus() {
        return productstatus;
    }

    public BookType getBooktype() {
        return booktype;
    }

    public String getCoverimage() {
        return coverimage;
    }

    public Object getMarkets() {
        return markets;
    }

    public Object getMarket_details() {
        return market_details;
    }

    @Override
    public String toString() {
        return "Product{" +
                "_id='" + _id + '\'' +
                ", title='" + title + '\'' +
                ", authors=" + authors +
                ", isbn='" + isbn + '\'' +
                ", language='" + language + '\'' +
                ", productstatus='" + productstatus + '\'' +
                ", booktype=" + booktype +
                ", coverimage='" + coverimage + '\'' +
                ", markets=" + markets +
                ", market_details=" + market_details +
                '}';
    }
}
