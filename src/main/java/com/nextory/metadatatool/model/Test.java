package com.nextory.metadatatool.model;

import org.springframework.data.annotation.Id;

public class Test {

    @Id
    private String _id;

    private String desc;

    public Test() {
    }

    public Test(String _id, String desc) {
        this._id = _id;
        this.desc = desc;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    @Override
    public String toString() {
        return "Test{" +
                "_id='" + _id + '\'' +
                ", desc='" + desc + '\'' +
                '}';
    }
}
