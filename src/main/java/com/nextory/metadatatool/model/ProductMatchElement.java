package com.nextory.metadatatool.model;

import com.nextory.metadatatool.data.elasticsearch.document.ElasticsearchProduct;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document
public class ProductMatchElement{
    private String bookId;
    private String title;
    private String isbn;
    private String coverImage;
    private String bookType;
    private String productStatus;
    private List<Author> authors;
    private String language;
    private List<ElasticsearchProduct> elasticsearchProductList;
    private ProductMatchElementStatus status;


    public ProductMatchElement() {
    }

    public ProductMatchElement(String bookId, String title, List<Author> authors,
                               String language, String isbn, List<ElasticsearchProduct> elasticsearchProductList,
                               String coverImage, String bookType, String productStatus) {
        this.bookId = bookId;
        this.title = title;
        this.authors = authors;
        this.language = language;
        this.isbn = isbn;
        this.coverImage = coverImage;
        this.bookType = bookType;
        this.productStatus = productStatus;
        this.elasticsearchProductList = elasticsearchProductList;
        this.status = ProductMatchElementStatus.PENDING;
    }

    public String getBookId() {
        return bookId;
    }

    public void setBookId(String bookId) {
        this.bookId = bookId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public List<ElasticsearchProduct> getElasticsearchProductList() {
        return elasticsearchProductList;
    }

    public void setElasticsearchProductList(List<ElasticsearchProduct> elasticsearchProductList) {
        this.elasticsearchProductList = elasticsearchProductList;
    }

    public String getCoverImage() {
        return coverImage;
    }

    public void setCoverImage(String coverImage) {
        this.coverImage = coverImage;
    }

    public ProductMatchElementStatus getStatus() {
        return status;
    }

    public void setStatus(ProductMatchElementStatus status) {
        this.status = status;
    }
}
