package com.nextory.metadatatool.executor;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class ReentrantCounter {
    private int counter;
    private final ReentrantReadWriteLock rwLock = new ReentrantReadWriteLock();
    private final Lock readLock = rwLock.readLock();
    private final Lock writeLock = rwLock.writeLock();

    public ReentrantCounter() {
        this.counter = 0;
    }

    public ReentrantCounter(int initialCounter) {
        this.counter = initialCounter;
    }

    public void incrementCounter() {
        writeLock.lock();
        try {
            counter += 1;
        } finally {
            writeLock.unlock();
        }
    }

    public void incrementCounter(int increment) {
        writeLock.lock();
        try {
            counter += increment;
        } finally {
            writeLock.unlock();
        }
    }

    public int getCounter(){
        readLock.lock();
        try {
            return counter;
        } finally {
            readLock.unlock();
        }
    }
}
