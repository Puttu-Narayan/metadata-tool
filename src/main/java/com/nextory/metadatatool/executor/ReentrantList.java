package com.nextory.metadatatool.executor;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class ReentrantList<E> {
    List<E> list;
    private final ReentrantReadWriteLock rwLock = new ReentrantReadWriteLock();
    private final Lock readLock = rwLock.readLock();
    private final Lock writeLock = rwLock.writeLock();

    public ReentrantList() {
        this.list = new ArrayList<>();
    }

    public ReentrantList(int size) {
        this.list = new ArrayList<>(size);
    }

    public List<E> getList() {
        readLock.lock();
        try{
            return list;
        } finally {
            readLock.unlock();
        }
    }

    public void addElementToList(E element) {
        writeLock.lock();
        try{
            this.list.add(element);
        } finally {
            writeLock.unlock();
        }
    }
}
