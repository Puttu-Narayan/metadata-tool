package com.nextory.metadatatool.executor;

import com.nextory.metadatatool.data.elasticsearch.document.ElasticsearchProduct;
import com.nextory.metadatatool.model.Product;
import com.nextory.metadatatool.model.ProductMatchElement;
import com.nextory.metadatatool.service.ElasticsearchService;
import com.nextory.metadatatool.service.MongoService;
import com.nextory.metadatatool.service.ProductMatchService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@Service
public class ProductMatchExecutorService extends ProductMatchService {
    ExecutorService executorService;
    ExecutorCompletionService executorCompletionService;
    private ReentrantCounter reentrantMongoPageCounter;
    private ReentrantCounter reentrantMongoSkipCounter;
    private ReentrantList<ProductMatchElement> reentrantProductMatchList;
    Runnable productMatchCommand;
    private static final int pageSize = 2000;

    /* CONSTRUCTORS */

    public ProductMatchExecutorService() {
        super();
        executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        executorCompletionService = new ExecutorCompletionService(executorService);
        productMatchCommand = new Runnable() {
            @Override
            public void run() {
                processPageProductMatchList();
            }
        };
    }

    public ProductMatchExecutorService(ElasticsearchService elasticsearchService, MongoService mongoService) {
        super(elasticsearchService, mongoService);
        executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        productMatchCommand = new Runnable() {
            @Override
            public void run() {
                processPageProductMatchList();
            }
        };
    }

    /* PUBLIC METHODS */

    public String getBookMatchListWithThreadPool(){
        resetExecutorService();
        createTasksForExecutorService();
        waitForExecutorServiceToFinish(5L, TimeUnit.HOURS);
        shutdownAndAwaitTermination();
        storeMatchResultsInDatabase();
        return gson.toJson(reentrantProductMatchList.getList());
    }

    /* PRIVATE METHODS */

    public void storeMatchResultsInDatabase() {
        for (ProductMatchElement element: reentrantProductMatchList.getList()){
            mongoService.save(element);
        }
    }

    private void processPageProductMatchList(){
        int page = reentrantMongoPageCounter.getCounter();
        reentrantMongoPageCounter.incrementCounter();
        long skip = reentrantMongoSkipCounter.getCounter();
        reentrantMongoSkipCounter.incrementCounter(MongoService.getPageSize());
        List<Product> productList = mongoService.getAllProductByPage(page, skip);
        fillProductMatchListWithProductList(productList);
        System.out.println("Processed "+skip+" elements");
    }

    private void fillProductMatchListWithProductList(List<Product> productList){
        List<ElasticsearchProduct> elasticsearchProductList;
        for(Product product: productList){
            elasticsearchProductList = getElasticSearchProductListByProduct(product);
            if (elasticsearchProductList != null && elasticsearchProductList.size()>0){
                reentrantProductMatchList.addElementToList(
                        new ProductMatchElement(
                                product.get_id(), product.getTitle(), product.getAuthors(), product.getLanguage()
                                , product.getIsbn(), elasticsearchProductList, product.getCoverimage(),
                                product.getBooktype().getBook_type(), product.getProductstatus())
                );
            }
        }
    }

    private void initializeReentrantVariables(int listSize){
        reentrantMongoPageCounter = new ReentrantCounter();
        reentrantMongoSkipCounter = new ReentrantCounter();
        reentrantProductMatchList = new ReentrantList(listSize);
    }

    private void createTasksForExecutorService(){
        int count = mongoService.getAllProductCount();
        int pageMax = count / pageSize;
        initializeReentrantVariables(count);
        int counter = 0;
        System.out.println("Final page iteration "+pageMax);
        do {
            try{
                executorService.execute(productMatchCommand);
                counter++;
            } catch (Exception e){
                executorService.shutdown();
                System.out.println(e.getMessage());
            }
        } while (counter<=pageMax);
    }

    private void waitForExecutorServiceToFinish(long time, TimeUnit unit){
        try {
            executorService.shutdown();
            executorService.awaitTermination(time, unit);
        } catch (InterruptedException e) {
            if(!Objects.isNull(executorService)) executorService.shutdown();
            e.printStackTrace();
        }
    }

    private void resetExecutorService(){
        if(Objects.isNull(executorService) || executorService.isTerminated() || executorService.isShutdown())
            executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
    }

    private void shutdownAndAwaitTermination() {
        executorService.shutdown(); // Disable new tasks from being submitted
        try {
            // Wait a while for existing tasks to terminate
            if (!executorService.awaitTermination(60, TimeUnit.SECONDS)) {
                executorService.shutdownNow(); // Cancel currently executing tasks
                // Wait a while for tasks to respond to being cancelled
                if (!executorService.awaitTermination(60, TimeUnit.SECONDS))
                    System.err.println("Pool did not terminate");
            }
        } catch (InterruptedException ie) {
            // (Re-)Cancel if current thread also interrupted
            executorService.shutdownNow();
            // Preserve interrupt status
            Thread.currentThread().interrupt();
        }
    }
}
