FROM openjdk:8
CMD java -version
ENV TZ 'Europe/Stockholm'
RUN echo $TZ > /etc/timezone && \
    apt-get update && apt-get install -y tzdata && \
    rm /etc/localtime && \
    ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && \
    dpkg-reconfigure -f noninteractive tzdata && \
apt-get clean
RUN date
COPY metadatatool-0.0.1-SNAPSHOT.war  application-v1.war
RUN wget -O dd-java-agent.jar 'https://dtdg.co/latest-java-tracer'
EXPOSE 8080/tcp
ENV DD_AGENT_HOST $DD_AGENT_SERVICE_HOST
ENV DD_AGENT_PORT $DD_AGENT_SERVICE_PORT
ENV TAKIPI_COLLECTOR_HOST=overops-prod-collector
ENV TAKIPI_COLLECTOR_PORT=6060
ENV JAVA_TOOL_OPTIONS=-agentpath:/takipi/lib/libTakipiAgent.so
RUN curl -sL https://s3.amazonaws.com/app-takipi-com/deploy/linux/takipi-agent-4.48.1.tar.gz | tar -xvzf -
ENTRYPOINT ["java","-XX:+UseG1GC","-XX:InitialHeapSize=3g","-XX:MaxHeapSize=3g","-XX:+UseStringDeduplication","-XX:+PrintGCDetails","-XX:+PrintGCTimeStamps","-XX:+PrintGCDateStamps","-Xloggc:/gc.log","-XX:+UseGCLogFileRotation","-XX:NumberOfGCLogFiles=5","-XX:GCLogFileSize=4M","-javaagent:/dd-java-agent.jar","-jar", "/application-v1.war"]